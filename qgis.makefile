GET_COMMON_OPTIONS  = -q -N
PACKAGE_NAME         = ax-qgis-ltstoshort
ANCIENCOMPAT         = 3.20.0-4
ANCIEN               = 3.20.0
VERSION              = 3.20.1
NUMBERV              = 2
VERSIONCOMPAT        := $(VERSION)-$(NUMBERV)
MSI                  = QGIS-OSGeo4W-$(VERSIONCOMPAT).msi 
DOWNLOAD_URLS        = https://qgis.org/downloads/$(MSI)
TARGET               = $(PACKAGE_NAME)_$(VERSION)-0.opsi

all:install
cd:
	cd /var/lib/opsi/workbench/$(PACKAGE_NAME)
	rm -rf CLIENT_DATA/files
#Ceci permet de changer le fichier control,setup et uninstall
sed:
	
	sed -i -e 's/$(ANCIENCOMPAT)/$(VERSIONCOMPAT)/g' CLIENT_DATA/setup.opsiscript
	sed -i -e 's/$(ANCIEN)/$(VERSION)/g' CLIENT_DATA/setup.opsiscript
	sed -i -e 's/$(ANCIEN)/$(VERSION)/g' CLIENT_DATA/uninstall.opsiscript
	sed -i -e 's/$(ANCIEN)/$(VERSION)/g' OPSI/control
#lea partie download prend le logiciel comme etant une msi
download:
	rm -rf CLIENT_DATA/files1	
	mkdir -p CLIENT_DATA/files1
	wget -q -P CLIENT_DATA/files1/ $(DOWNLOAD_URLS)


build: $(TARGET)
	opsi-makepackage
install: 
	opsi-package-manager -p ask -i $(TARGET)
clean:
	rm -rf CLIENT_DATA/files
	rm -f $(TARGET)

